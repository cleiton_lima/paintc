all:
	gcc -c src/funcoes.c -o src/funcoes.o
	ar rcs libfuncoes.a src/funcoes.o
	gcc principal.c -c -o principal.o
	gcc principal.o -lfuncoes -L. -o bin/exec

run:
	./exec

clean:
	rm .src/*.o