#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// #include </home/cleiton-lima/Documentos/2019.2/ITP/libgraph-1.0.2/graphics.h>
#include "src/tipos.h" //chama os tipos criados 
#include "src/funcoes.h" //chama as funções criadas

void main()
{
	Imagem *imagem;
	char nomeImagem[20];
	char nomeCompleto[50];
	char nomeImagemNova[20];
	char nomeCompletoNovo[50];
	int colunas, linhas, variacaoCor, qtdPixel, cond;
	// int rlin0, rcol0, rlin1, rcol1;
	char formato[3];
	Pixel *vetPixel;

	// Laço destinado a exibir para o usuário as opções de criação de imagens
	do
	{
		printf("\nInsira o número da ação que deseja executar: \n"
			"1 - Gerar imagem no formato PPM\n"
			"2 - Gerar uma reta\n"
			"0 - Para Sair\n");
		scanf("%d", &cond);

		// Verifica qual opção foi selecionada pelo usuário e chama a função correspondente
		switch (cond)
		{
		case 1:
			caminhoImagem(nomeImagem, nomeCompleto, nomeImagemNova, nomeCompletoNovo);
            lerImagem(nomeCompleto, &colunas, &linhas, &vetPixel, &variacaoCor, &qtdPixel, formato);
			salvarImagem(nomeCompletoNovo, colunas, linhas, vetPixel, variacaoCor, qtdPixel);
			break;
		
		// case 2:
			
     		// printf("Insira a coordenada do eixo X do ponto inicial:\n");
     		// scanf("%d", &rlin0);
      		// printf("Insira a coordenada do eixo Y do ponto inicial:\n");
      		// scanf("%d", &rcol0);
     	 	// printf("Insira a coordenada do eixo X do ponto final: \n");
     		// scanf("%d", &rlin1);
      		// printf("Insira a coordenada do eixo Y do ponto final:\n");
      		// scanf("%d", &rcol1);
			// caminhoImagem(nomeImagem, nomeCompleto, nomeImagemNova, nomeCompletoNovo);
			// retas(rlin0, rcol0, rlin1, rcol1);
		}
	} while ( cond != 0);
}