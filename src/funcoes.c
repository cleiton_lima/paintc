#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "tipos.h"

// Esta função lê o nome da imagem que o usuario deseja editar e pede para inserir o nome da nova imagem que deve ser gerada e em seguida
// formar o caminho completo de onde a imagem a ser lida está e onde a imagem gerada vai ficar. 
void caminhoImagem(char nomeImagem[], char nomeCompleto[],char nomeImagemNova[],char nomeCompletoNovo[])
{
    printf("Digite o nome da imagem que deseja ler:");
    scanf("%s", nomeImagem);
    strcpy(nomeCompleto, "imagens/"); 
    strcat(nomeCompleto, nomeImagem); 
    strcat(nomeCompleto, ".ppm");
    printf("Digite o nome da imagem que deseja imprimir:");
    scanf("%s", nomeImagemNova);
    strcpy(nomeCompletoNovo, "imagens/"); 
    strcat(nomeCompletoNovo, nomeImagemNova); 
    strcat(nomeCompletoNovo, ".ppm");
    
}

// Lê o conteúdo da imagem e armazena nas variáveis contidas na função principal main onde serão passadas como parâmetros nas funções
// quando o usuário aplicar algum dos filtos 
void lerImagem(char nomeCompleto[], int *colunas, int *linhas, Pixel **vetPixel, int *variacaoCor, int *qtdPixel, char formato[])
{
	FILE *arquivo;
	arquivo = fopen(nomeCompleto, "r");

	if(arquivo == NULL)
	{
		printf("\n O arquivo não foi lido! \n");
		exit(0);
	}

	fscanf(arquivo, "%s", formato);
	fscanf(arquivo, "%i %i", colunas, linhas);
	fscanf(arquivo, "%i", variacaoCor);

	*qtdPixel = (*colunas) * (*linhas);
	*vetPixel = malloc(*qtdPixel * sizeof(Pixel));

	int i;
	for (int i = 0; i< *qtdPixel; i++)
	{
		fscanf(arquivo, "%i", &(*vetPixel)[i].r);
		fscanf(arquivo, "%i", &(*vetPixel)[i].g);
		fscanf(arquivo, "%i", &(*vetPixel)[i].b);
	}
// Lê o conteúdo da imagem e armazena nas variáveis contidas na função principal main onde serão passadas como parâmetros nas funções
// quando o usuário aplicar algum dos filtos 
	fclose(arquivo);
}


// Essa função tem o papel de criar uma nova imagem e salvá-la no formato .ppm
void save(char nomeCompletoNovo[],int colunas,int linhas,Pixel *vetPixel,int variacaoCor, int qtdPixel)
{
    
    FILE *file = fopen(nomeCompletoNovo, "w"); // criando um novo arquivo
    qtdPixel = (colunas) * (linhas); // definindo a quantidade de pixel que terá o vetor
    fprintf(file, "P3\n"); //escrever o formato do arquivo
    fprintf(file, "%d %d\n", colunas, linhas); //tamanho da imagem
    fprintf(file, "%d\n", variacaoCor); // escrevendo a variação de cor
    // percorrendo o vetor
     for (int i = 0; i < qtdPixel; i++)
        {
            // escrevendo o vetor no arquivo
            fprintf(file,"%d\n", (vetPixel)[i].r);
            fprintf(file,"%d\n", (vetPixel)[i].g);
            fprintf(file,"%d\n", (vetPixel)[i].b);

        }
    // fechando o arquivo e limpando o espaço da memória para que seja possivel ler outro arquivo em seguida
    // usando o mesmo vetor
    fclose(file);
    free(vetPixel);

}

// link referência função.
//https://www.codingalpha.com/bresenham-line-drawing-algorithm-c-program/

void rect(int rlin0, int rcol0, int rlin1, int rcol1)
{
      int dx, dy;
      int tmp, x, y;
      dx = rlin1 - rlin0; //determinamos o tamanho da linha X.
      dy = rcol1 - rcol0; // mesma so que pra y.
      x = rlin0;
      y = rcol0;
      tmp = 2 * dy - dx;
      while(x < rlin1)
      {
            if(tmp < 0)
            {
                  tmp = tmp + 2 * dy;
            }
            else
            {
                  y = y + 1;
                  tmp = tmp + 2 * dy - 2 * dx; //distancia de pontos formula.
            }
            putpixel(x, y, 6);
            x++;
      }
}

// void polygon()
// {

// }

void circle (int xlin, int ycol, int raio)
{
    int xl = -raio, yc = 0, form = 2 * M_PI * raio;

    do
    {
        //Representa todos os quadrantes do plano cartesiano
        qtdPixel(xlin - xl, ycol + yc);
        qtdPixel(xlin - yc, ycol - xl);
        qtdPixel(xlin + xl, ycol - yc);
        qtdPixel(xlin + yc, ycol + xl);

        raio = form;
         if (raio <= yc) 
            form += ++yc * 2 + 1;
        if (raio > xl || form > yc)
            form += ++xl * 2 + 1;
    } while (xl < 0);
}

