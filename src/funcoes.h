#ifndef FUNCOES_H
#define FUNCOES_H
#include <stdio.h>
#include <stdlib.h>
#include "funcoes.h"

// Essa função lê os nomes das imagens e gera os caminhos onde elas estão e deverão ser salvas
void caminhoImagem(char nomeImagem[], char nomeCompleto[], char nomeImagemNova[], char nomeCompletoNovo[]);

// Essa função lê os dados da imagem que será editada
void lerImagem(char nomeCompleto[], int *colunas, int *linhas, Pixel **vetPixel, int *variacaoCor, int *qtdPixel, char formato[]);

// Essa função salva a imagem que foi lida como uma cópia
void save(char nomeCompletoNovo[], int colunas, int linhas, Pixel *vetPixel, int variacaoCor, int qtdPixel);

// Essa função desenha uma reta a partir das coordenadas dos pontos 
void rect(int rlin0, int rcol0, int rlin1, int rcol1);

// void polygon();

void circle(int xl, int yc, int raio);

// void fill();

// void open();

// void color();

// void clear();

#endif