#ifndef TIPOS
#define TIPOS

// Tipos definidos para as cores
typedef struct Pixels
{
    int r, g, b;
} Pixel;

// Struct usada para geração de imagens
typedef struct Imagens
{
	char *formato;
	int altura, largura;
	int varCor;
	Pixel *Pixels;
	int qtdPixel; 
} Imagem;


#endif